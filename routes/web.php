<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contact', function () {
    return view('contact');
})->name('contact');
Route::post('/contact', 'HomeController@contact');

Route::get('/courses', 'CourseController@all');
Route::get('/courses/{course}', 'CourseController@showCourses')->name('courses.show');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('/suscriptions', 'SuscriptionController@index');
    Route::get('/courses/categories', 'CourseCategoryController@index')->name('admin.courses.categories');
    Route::post('/courses/categories', 'CourseCategoryController@store')->name('admin.courses.categories');

    Route::get('/courses', 'CourseController@index')->name('admin.courses');
    Route::get('/courses/create', 'CourseController@create')->name('admin.courses.add');
    Route::post('/courses/categories', 'CourseController@store')->name('admin.courses');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


