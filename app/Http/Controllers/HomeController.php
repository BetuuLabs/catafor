<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function contact(Request $request)
    {
        $request->validate([
           'name' => 'required',
           'email' => 'required',
           'content' => 'required'
        ]);

        DB::table('contact')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'content' => $request->message
        ]);

        return redirect()->back();
    }
}
